class Review < ActiveRecord::Base
	belongs_to :movie

	validates :name, :stars, :comment, presence: true
	validates :stars, numericality: true, inclusion: { in: 1...5 }
	validates :comment, length: { minimum: 4 }
end
