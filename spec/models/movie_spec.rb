require 'spec_helper'

describe Movie do

  it { should validate_presence_of(:title) }
  it { should validate_presence_of(:released_on) }
  it { should validate_presence_of(:duration) }
  it { should ensure_length_of(:description).is_at_least(25) }
  it { should have_many(:reviews)}
  
  describe "image_file_name validations" do
    context "with a valid image_file_name" do
      %w(png gif jpg).each do |type|
        subject(:movie) { FactoryGirl.build(:movie, image_file_name: "test.#{type}")}
        it { should be_valid }
      end
    end
    context "with an invalid image_file_name" do
      let(:movie) { FactoryGirl.build(:movie, image_file_name: 'test.pdf') }
      subject { movie } 
      it { should_not be_valid }
      describe "validation messages" do
        before { movie.save }
        subject { movie.errors.messages[:image_file_name] }
        it { should include "must reference a GIF, JPG, or PNG image" }
      end
    end
  end

  describe "rating validations" do
    RATINGS = %w(G PG PG-13 R NC-17)
    context "with a valid rating" do
      RATINGS.each do |rating|
        subject { FactoryGirl.build(:movie, rating: rating) }
        it { should be_valid }
      end
    end

    context "with an invalid rating" do
      let(:movie) { FactoryGirl.build(:movie, rating: "F") }
      subject { movie }
      it { should_not be_valid }

    end
  end

  describe "#flop" do
    it "is a flop if the total gross is less than $50M" do
      movie = FactoryGirl.build(:movie, total_gross: 30000000.00)
      expect(movie).to be_flop
    end

    it "is not a flop if the total gross is greater than $50M" do
      movie = FactoryGirl.build(:movie, total_gross: 60000000.00)
      expect(movie).not_to be_flop  
    end
  end
  describe ".released" do
    it "includes only released movies" do
      movie = FactoryGirl.create(:movie, released_on: 1.month.from_now)
      expect(Movie.released).not_to include(movie)
    end

    it "orders movies by released date newest first" do
      old_movie = FactoryGirl.create(:movie, released_on: 5.days.ago)
      new_movie = FactoryGirl.create(:movie, released_on: 1.day.ago)

      expect(Movie.released.count).to eq 2
      expect(Movie.released.first).to eq new_movie
      expect(Movie.released.last).to eq old_movie
    end
  end

  describe "with reviews" do
    
    subject(:movie) { FactoryGirl.create(:movie_with_reviews) }
    
    its(:reviews) { should have(3).items }
    it "deletes reviews when the movie is deleted" do
      expect{ movie.delete }.to change(Review, :count).by(3)
    end
  end
end
