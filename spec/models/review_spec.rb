describe Review do
	it { should respond_to(:name) }
	it { should respond_to(:stars) }
	it { should respond_to(:comment) }
	it { should belong_to(:movie) }

	it { should validate_presence_of(:name) }
	it { should validate_presence_of(:stars) }
	it { should validate_presence_of(:comment) }
	it { should ensure_inclusion_of(:stars).in_range(1...5) }
	it { should ensure_length_of(:comment).is_at_least(4) }

	describe "deleting a review" do
		let!(:review) { create(:review) }
		it "does not delete the associated movie" do
			expect { review.delete }.not_to change(Movie, :count)
		end
	end
end