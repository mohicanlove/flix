require 'spec_helper'

describe 'editing a form' do
  let(:movie) { Movie.create(movie_attributes) }
  before(:each) { visit edit_movie_path(movie) }
  
  it 'shows an edit form' do
    expect(find_field('Title').value).to eq(movie.title)
    expect(find_field('Description').value).to eq(movie.description)
    expect(find_field('Rating').value).to eq(movie.rating)
  end
  
  context 'when submitted with valid data' do
    it 'updates the movie when the form is submitted' do
      fill_in 'Title', with: 'Iron Man 2'
      click_button 'Update Movie'

      expect(page).to have_text('Iron Man 2')
      expect(page).to have_text('Movie successfully updated!')
      expect(current_path).to eq(movie_path(movie))

    end
  end

  context 'when submitted with invalid data' do
    it 'displays the form' do
      fill_in 'Title', with: ''
      click_button 'Update Movie'
      expect(page).to have_text('Title can\'t be blank')
    end
  end
end
