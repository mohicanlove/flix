require 'spec_helper'

describe "Viewing the list of movies" do
  let!(:movies) { create_list(:movie, 3, total_gross: 60_000_000) } 
  before(:each) do
    visit movies_url
  end
  subject { page }
  context "with released movies" do
    it { should have_text(movies.first.title) }
    it { should have_text(movies.second.title) }
    it { should have_text(movies.last.title) }

    it { should have_text(movies.first.description[0..9]) }
    it { should have_text(movies.first.released_on.strftime("%Y")) }
    it { should have_text(movies.first.rating) }
    it { should have_text(number_to_currency(movies.first.total_gross)) }

    describe "sorting" do
      it "sorts the movies newest to oldest" do
        expect(page.body.index(movies.first.title)).to be < page.body.index(movies.second.title)
      end
    end
  end

  context "with an unreleased movie" do
    let!(:movie) { create(:movie, { released_on: 1.month.from_now }) }
    it { should_not have_text(movie.title) }
  end

  it { should have_link('Add New Movie') }
end
