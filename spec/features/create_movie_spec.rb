require 'spec_helper'

describe 'creating a movie' do
  before(:each) { visit new_movie_path }
  context 'with valid data' do
    it 'shows a create form' do
      fill_in 'Title', with: 'Iron Man'
      fill_in 'Description', with: 'Tony Stark builds an armored suit to fight the throes of evil'
      select 'PG-13', from: 'movie_rating'
      select '2008', from: 'movie[released_on(1i)]'
      select 'May', from: 'movie[released_on(2i)]'
      select '28', from: 'movie[released_on(3i)]'
      fill_in 'Total gross', with: '318412101.00'
      fill_in 'Cast', with: 'The award-winning cast'
      fill_in 'Director', with: 'The ever-creative director'
      fill_in 'Duration', with: '123 min'
      fill_in 'Image file name', with: 'movie.png'
      expect { click_button 'Create Movie' }.to change(Movie, :count).by(1)

      expect(page).to have_text('Iron Man')
      expect(page).to have_text('Movie successfully created!')
    end
  end

  context 'with invalid data' do
    describe 'submitting the form without any data' do    
      it 'redirects back to the create form' do
        expect{ click_button 'Create Movie' }.not_to change(Movie, :count)
      end
    
      describe "validation messages" do
        before { click_button 'Create Movie' }
        subject { page }
        it { should have_text('Title can\'t be blank') }
        it { should have_text('Duration can\'t be blank') }
        it { should have_text('Description is too short') }
      end
    end
  end
end
