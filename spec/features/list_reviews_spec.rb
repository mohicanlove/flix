require "spec_helper"
describe "listing reviews" do
	let!(:review) { create(:review, { stars: 3 }) }
	before { visit movie_reviews_path(review.movie) }
	subject { page }
	
	it { should have_text("3 stars by #{review.name}: \"#{review.comment}\"") }
end