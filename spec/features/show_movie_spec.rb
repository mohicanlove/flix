require 'spec_helper'

describe "Viewing a single movie" do
  let!(:movie) { create(:movie, { total_gross: 318_412_101 }) }
  before(:each) { visit movie_path(movie) }
  subject { page }

  it { should have_text(movie.title) }
  it { should have_text(movie.description) }
  it { should have_text(movie.released_on) }
  it { should have_text(movie.rating) }
  it { should have_text("$318,412,101.00") }
  it { should have_text(movie.cast) }
  it { should have_text(movie.director) }
  it { should have_text(movie.duration) }
  it { should have_selector("img[src$='#{movie.image_file_name}']") }
  it { should have_link("#{pluralize(movie.reviews.count, 'review')}", href: movie_reviews_path(movie)) }
  
  context "when the total gross exceeds $50M" do
    let!(:movie) { create(:movie, { total_gross: 60_000_000 }) }
    it { should have_text("$60,000,000.00") }
  end
  
  context "when the total gross is less than $50M" do
    let!(:movie) { create(:movie, { total_gross: 40_000_000 }) }
    it { should have_text("Flop!") }
  end

  context "when there is no image file" do
    let!(:movie) { create(:movie, { image_file_name: "" }) }
    it { should have_selector("img[src$='placeholder.png']") }
  end
end
