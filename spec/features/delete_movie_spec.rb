require 'spec_helper'

describe 'Deleting a movie', js: true do
  let!(:movie) { Movie.create(movie_attributes) }
  before(:each) { visit movie_path(movie) }
  describe "confirmation prompt" do
  	context "when dismissed" do
  		it "does not delete the movie" do
  			page.driver.dismiss_js_confirms!
		    click_link 'Delete'
		  	expect(page.driver.confirm_messages).to include('Are you sure?')
		  	expect(page).to have_text(movie.title)
  		end
  	end

  	context "when confirmed" do
	    before(:each) do
	    end
	    it "deletes the movie and redirects to the index view" do
		    expect{ 
		    	click_link 'Delete'
  				page.driver.accept_js_confirms!
	    	}.to change(Movie, :count).by(-1)
		    expect(current_path).to eq(movies_path)
		    expect(page).to have_text('MOVIE SUCCESSFULLY DELETED!')
	  	end
  	end
  end
end
