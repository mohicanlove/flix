require 'spec_helper'

describe MoviesHelper do
  describe "#format_total_gross" do
    context "with a movie that is a flop" do
      it "returns the word 'flop'" do
        movie = stub(flop?: true)
        expect(helper.format_total_gross(movie)).to include('Flop!')  
      end
    end

    context "with a movie that is not a flop" do
      it "returns the total formatted as currency" do
        movie = stub(flop?: false, total_gross: "100")
        expect(helper.format_total_gross(movie)).to eq("$100.00")
      end
    end
  end

  describe "#image_for" do
    context "with a movie that has an image file name" do
      it "returns the file name" do
        movie = stub(image_file_name: "example.png")
        expect(helper.image_for(movie)).to include("example.png")
      end
    end
    
    context "with a movie that does not have an image file name" do
      it "returns the placeholder file name" do
        movie = stub(image_file_name: "")
        expect(helper.image_for(movie)).to include("placeholder.png")
      end
    end
  end
end
