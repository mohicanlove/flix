FactoryGirl.define do
	factory :review do
    name { Faker::Name.name }
    stars { SecureRandom.random_number(4) + 1 }
    comment { Faker::Lorem.sentence }
    movie
	end
end