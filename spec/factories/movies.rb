FactoryGirl.define do
	factory :movie do
		title { Faker::Lorem.sentence }
		description Faker::Lorem.paragraph
		director { Faker::Name.name }
    rating "PG-13"
		total_gross { SecureRandom.random_number(1_000_000) }
    released_on "2008-05-02"
    cast "Faker::Lorem.sentence"
    duration "#{SecureRandom.random_number(200)} min"
    image_file_name { "#{Faker::Name.first_name}.jpg" }


    factory :movie_with_reviews do
    	ignore do
    		reviews_count 3
    	end
    	after(:create) do |movie, evaluator|
    		create_list(:review, evaluator.reviews_count, movie: movie)
    	end
    end
	end
end