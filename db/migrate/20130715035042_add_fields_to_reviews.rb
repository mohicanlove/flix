class AddFieldsToReviews < ActiveRecord::Migration
  def change
    add_column :reviews, :name, :string
    add_column :reviews, :stars, :integer
    add_column :reviews, :comment, :text
  end
end
